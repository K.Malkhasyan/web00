
<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  $messages = array(); 
  if (!empty($_COOKIE['save'])) { 
    setcookie('save', '', 100000);
    $messages[] = 'Результаты сохранены.';
  }

  
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['limb'] = !empty($_COOKIE['limb_error']);
  $errors['super'] = !empty($_COOKIE['super_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);

  if ($errors['name']) {
    setcookie('name_error', '', 100000);
    $messages[] = '<div class="error">Укажите имя.</div>';
  }

  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages[] = '<div class="error">Адрес эл.почты указан неверно.</div>';
  }

  if ($errors['date']) {
    setcookie('date_error', '', 100000);
    $messages[] = '<div class="error">Дата рождения указана неверно.</div>';
  }
  
  if ($errors['gender']) {
    setcookie('gender_error', '', 100000);
    $messages[] = '<div class="error">Укажите пол.</div>';
  }

  if ($errors['limb']) {
    setcookie('limb_error', '', 100000);
    $messages[] = '<div class="error">Укажите число конечностей.</div>';
  }

  if ($errors['super']) {
    setcookie('super_error', '', 100000);
    $messages[] = '<div class="error">Укажите суперспособности.</div>';
  }

  if ($errors['check']) {
    setcookie('check_error', '', 100000);
    $messages[] = '<div class="error">Пожалуйста, ознакомьтесь с контрактом.</div>';
  }

  $values = array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
  $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
  $values['limb'] = empty($_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];
  $values['super1'] = empty($_COOKIE['super1_value']) ? '' : $_COOKIE['super1_value'];
  $values['super2'] = empty($_COOKIE['super2_value']) ? '' : $_COOKIE['super2_value'];
  $values['super3'] = empty($_COOKIE['super3_value']) ? '' : $_COOKIE['super3_value'];
  $values['message'] = empty($_COOKIE['message_value']) ? '' : $_COOKIE['message_value'];
  $values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];

  include('form.php');
}
else {
  $errors = FALSE;
  if (empty($_POST['name'])) {
    setcookie('name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('name_value', $_POST['name'], time() + 365 * 30 * 24 * 60 * 60);
  }

  if (!preg_match("|^[-0-9a-z_\.]+@[-0-9a-z_^\.]+\.[a-z]{2,6}$|i", $_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 365 * 30 * 24 * 60 * 60);
  }

  if (!preg_match('/^(\d{1,2})\.(\d{1,2})(?:\.(\d{4}))?$/', $_POST['date'])) {
    setcookie('date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('date_value', $_POST['date'], time() + 365 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['gender'])) {
    setcookie('gender_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('gender_value', $_POST['gender'], time() + 365 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['limb'])) {
    setcookie('limb_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('limb_value', $_POST['limb'], time() + 365 * 30 * 24 * 60 * 60);
  }

  if (!isset($_POST['super1']) && !isset($_POST['super2']) && !isset($_POST['super3']) && !isset($_POST['super4'])) {
    setcookie('super_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('super1_value', isset($_POST['super1']) ? $_POST['super1'] : '', time() + 365 * 30 * 24 * 60 * 60);
    setcookie('super2_value', isset($_POST['super2']) ? $_POST['super2'] : '', time() + 365 * 30 * 24 * 60 * 60);
    setcookie('super3_value', isset($_POST['super3']) ? $_POST['super3'] : '', time() + 365 * 30 * 24 * 60 * 60);
    setcookie('super4_value', isset($_POST['super4']) ? $_POST['super4'] : '', time() + 365 * 30 * 24 * 60 * 60);
  }

  if (!empty($_POST['message'])) {
    setcookie('message_value', $_POST['message'], time() + 365 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['check'])) {
    setcookie('check_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('check_value', $_POST['check'], time() + 365 * 30 * 24 * 60 * 60);
  }

  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('limb_error', '', 100000);
    setcookie('super_error', '', 100000);
    setcookie('check_error', '', 100000);
    
    $user = 'u16416';
    $pass = '9953732';
    $db = new PDO('mysql:host=localhost;dbname=u16416', $user, $pass);

    $name = $_POST['name'];
    $email = $_POST['email'];
    $date = $_POST['date'];
    $gender = $_POST['gender'];
    $limb = $_POST['limb'];
    $super = $_POST['super1'].
      (isset($_POST['super2']) ? (' '. $_POST['super2']) : '').
      (isset($_POST['super3']) ? (' '. $_POST['super3']) : '').
    $message = $_POST['message'];

    try {
      $stmt = $db->prepare("INSERT INTO anketa (name, email, date, gender, limb, super, message) VALUES (:name, :email, :date, :gender, :limb, :super, :message)");
      $stmt->bindParam(':name', $name);
      $stmt->bindParam(':email', $email);
      $stmt->bindParam(':date', $date);
      $stmt->bindParam(':gender', $gender);
      $stmt->bindParam(':limb', $limb);
      $stmt->bindParam(':super', $super);
      $stmt->bindParam(':message', $message);
      $stmt->execute();
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
  }
  setcookie('save', '1');

  header('Location: index.php');
}
